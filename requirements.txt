blinker
Genshi>=0.6
Jinja2>=2.6
PyWebDAV>=0.9.8
Pygments>=1.6
Sphinx>=1.1.3
argparse>=1.2.1
caldav>=0.1.12
--allow-external cdecimal
#cdecimal>=2.3
-e git+https://github.com/midlgxdev/cdecimal-2.3.git#egg=cdecimal
chardet>=2.1.1
coverage>=3.6
docutils>=0.10
easyimap>=0.3.3
imaplib2
jsonrpclib
lxml>=3.1.0
nose>=1.2.1
polib>=1.0.2
psycopg2>=2.4.6
pydot
# require freetds-dev
pymssql
pyparsing>=1.5.6
python-dateutil>=2.1
python-ldap>=2.4.10
python-nmap
-e hg+http://hg.tryton.org/python-sql/#egg=python-sql-0.8
python-stdnum>=0.9
pytz>=2012h
pyusb>=1.0.0b1
pyserial>=2.7
#--allow-unverified PIL
#--allow-external PIL
#PIL==1.1.7
pillow
relatorio>=0.5.6
simplejson>=2.6.2
six>=1.2.0
vobject>=0.8.1c
wsgiref>=0.1.2
yolk>=0.4.3
openpyxl>=2.1.1
barcodenumber>=0.1
vatnumber
mock
html2text
goocalendar
emailvalid
simpleeval
pyflakes
flake8
suds>=0.4
PyPDF2

#For the scenarios
behave

#nereid
wtforms-recaptcha
Flask-Babel
Flask-WTF>=0.9.4
pyaml>=13.12.0


xlrd

#retrofix
-e hg+https://hg@bitbucket.org/trytonspain/python-retrofix#egg=retrofix
-e hg+https://hg@bitbucket.org/trytonspain/python-banknumber#egg=banknumber

# galatea
fabric
slug
